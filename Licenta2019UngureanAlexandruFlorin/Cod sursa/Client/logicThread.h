#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <QThread>
#include <QtNetwork/QTcpSocket>

#define CONNECTION_TIMEOUT 3000
#define READ_TIMEOUT 5000

class MainWindow;

class LogicThread : public QThread
{
public:
    LogicThread(MainWindow* mainThreadPointer, QString username, QString password);
    LogicThread(QString username, QString password, QString problem_name, QString code);
    LogicThread(MainWindow* mainThreadPointer, QString username, QString password, QString action);
    void run();

private:
    void sendData(QByteArray data);

    QTcpSocket* clientSocket;
    QString code;
    QString problem_name;
    MainWindow* mainThreadPointer;

    int mode;

    QString username_;
    QString password_;
};

#endif // CLIENTTHREAD_H
