#include <QHBoxLayout>

#include <QApplication>
#include <QScreen>
#include <QDesktopWidget>

#include "mainwindow.h"
#include "logicThread.h"

std::vector<Problem> MainWindow::problems;
QString MainWindow::score = "";
bool MainWindow::conditionVariable = false;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    init();
}

int MainWindow::loggedIn = 0;
int MainWindow::registered = 0;

void MainWindow::init()
{
    loggedIn = 0;
    registered = 0;
    file_as_source_ = false;
    mainPanelInitialized_ = false;
    loginOrRegisterPanelInitialized_ = false;

    initializeLoginOrRegisterPanel();
}

void MainWindow::initializeMainPanel()
{
    qDebug() << mainPanelInitialized_;
    if (!mainPanelInitialized_)
    {
        mainPanelInitialized_ = true;

        LogicThread *logicThread = new LogicThread(this, username_, password_);
        logicThread->start();

        while(!MainWindow::conditionVariable)
        {
            // wait for client side data to come
            QThread::msleep(100);
            qDebug() << "...";
        }

        QScreen *screen = QGuiApplication::primaryScreen();
        QRect  screenGeometry = screen->geometry();
        screenHeight = screenGeometry.height();
        screenWidth = screenGeometry.width();

        showMaximized();

        mainPanel_ = new QWidget();
        root_layout_ = new QHBoxLayout();
        layout_ = new QVBoxLayout();

        load_file_button_ = new QPushButton("Load source file");
        send_code_button_ = new QPushButton("Send code");
        source_code_text_ = new QTextEdit();
        highlighter = new CodeHighlighter(source_code_text_->document());

        status_label_ = new QTextEdit();

        descriptionArea_ = new QTextEdit();

        problemsScrollArea_ = new QScrollArea();
        scrollAreaRootWidget = new QWidget();
        scrollAreaLayout = new QVBoxLayout();

        scrollAreaLayout->setAlignment(Qt::AlignTop);

        source_code_text_->setStyleSheet("QTextEdit"
                                         "{"
                                            "color: white;"
                                         "}");

        status_label_->setStyleSheet("QTextEdit"
                                    "{"
                                        "color: white;"
                                    "}");

        descriptionArea_->setStyleSheet("QTextEdit"
                                    "{"
                                        "color: white;"
                                    "}");

        QString buttonCss = "QPushButton"
                            "{"
                                "background-color: #555871;"
                                "color: white;"
                                "border-radius: 10px;"
                                "border: 2px solid black;"
                            "}";

        this->setStyleSheet("background-color: #26283B;");

        load_file_button_->setStyleSheet(buttonCss);
        send_code_button_->setStyleSheet(buttonCss);

        connect(load_file_button_, SIGNAL (clicked()), this, SLOT(getFileContent()));
        connect(send_code_button_, SIGNAL (clicked()), this, SLOT(sendCode()));

        layout_->addWidget(source_code_text_);
        layout_->addWidget(load_file_button_);
        layout_->addWidget(send_code_button_);
        layout_->addWidget(status_label_);
        layout_->addWidget(descriptionArea_);

        QFont codeEditorFont;
        codeEditorFont.setFamily("Courier New");
        codeEditorFont.setStyleHint(QFont::Monospace);
        codeEditorFont.setFixedPitch(true);
        codeEditorFont.setPointSize(10);

        source_code_text_->setAcceptRichText(true);
        source_code_text_->setFont(codeEditorFont);

        int tabSize = 4;
        QFontMetrics metrics(source_code_text_->font());
        source_code_text_->setTabStopWidth(tabSize * metrics.width(' '));

        root_layout_->addLayout(layout_);
        root_layout_->addWidget(problemsScrollArea_);
        mainPanel_->setLayout(root_layout_);

        problemsScrollArea_->setWidget(scrollAreaRootWidget);
        scrollAreaRootWidget->setLayout(scrollAreaLayout);

        status_label_->setText("Result:");
        status_label_->setMaximumHeight(screenHeight / 7);
        status_label_->setReadOnly(true);

        descriptionArea_->setText("Description:");
        descriptionArea_->setMaximumHeight(screenHeight / 10);
        descriptionArea_->setReadOnly(true);

        problemsScrollArea_->setWidgetResizable(true);

        problemsScrollArea_->setMaximumWidth(screenWidth / 5);
        problemsScrollArea_->setStyleSheet("QScrollArea"
                            "{"
                                "background-color: #2E303E;"
                            "}");
    }
    setCentralWidget(mainPanel_);

    connect(source_code_text_, SIGNAL(textChanged()), this, SLOT(onSourceCodeChanged()));

    addProblemToProblemsPanel();
}

void MainWindow::initializeLoginOrRegisterPanel()
{
    if (!loginOrRegisterPanelInitialized_)
    {
        loginOrRegisterPanelInitialized_ = true;

        loginOrRegisterTitle_ = new QLabel("\tLogin or register");

        loginOrRegisterPanel_ = new QWidget();
        usernamePanel_ = new QWidget();
        passwordPanel_ = new QWidget();

        loginOrRegisterVLayout_= new QVBoxLayout();
        usernameLayout_ = new QHBoxLayout();
        passwordLayout_ = new QHBoxLayout();

        usernameLabel_ = new QLabel("Username: ");
        usernameInput_ = new QLineEdit();

        passwordLabel_ = new QLabel("Password: ");
        passwordInput_ = new QLineEdit();

        loginButton_ = new QPushButton("Login");
        registerButton_ = new QPushButton("Register");
        statusLabel_ = new QLabel();

        passwordInput_->setEchoMode(QLineEdit::Password);

        this->setStyleSheet("background-color: #26283B;");

        QString buttonCss = "QPushButton"
                            "{"
                                "background-color: #555871;"
                                "color: white;"
                                "border-radius: 10px;"
                                "border: 2px solid black;"
                            "}";

        QString labelCss = "QLabel"
                           "{"
                                "color: white;"
                           "}";

        QString lineEdit = "QLineEdit"
                           "{"
                                "color: white;"
                           "}";

        loginButton_->setStyleSheet(buttonCss);
        registerButton_->setStyleSheet(buttonCss);

        loginOrRegisterTitle_->setStyleSheet(labelCss);
        usernameLabel_->setStyleSheet(labelCss);
        passwordLabel_->setStyleSheet(labelCss);

        usernameInput_->setStyleSheet(lineEdit);
        passwordInput_->setStyleSheet(lineEdit);

        usernameLayout_->addWidget(usernameLabel_);
        usernameLayout_->addWidget(usernameInput_);
        usernamePanel_->setLayout(usernameLayout_);

        passwordLayout_->addWidget(passwordLabel_);
        passwordLayout_->addWidget(passwordInput_);
        passwordPanel_->setLayout(passwordLayout_);

        loginOrRegisterVLayout_->addWidget(loginOrRegisterTitle_);
        loginOrRegisterVLayout_->addWidget(usernamePanel_);
        loginOrRegisterVLayout_->addWidget(passwordPanel_);
        loginOrRegisterVLayout_->addWidget(loginButton_);
        loginOrRegisterVLayout_->addWidget(registerButton_);
        loginOrRegisterVLayout_->addWidget(statusLabel_);

        loginOrRegisterPanel_->setLayout(loginOrRegisterVLayout_);

        connect(loginButton_, SIGNAL (clicked()), this, SLOT(login()));
        connect(registerButton_, SIGNAL (clicked()), this, SLOT(createAccount()));
    }

    setCentralWidget(loginOrRegisterPanel_);
}

void MainWindow::login()
{
    username_ = usernameInput_->text();
    password_ = passwordInput_->text();

    LogicThread *logicThread = new LogicThread(this, username_, password_, "Login");
    logicThread->start();

    while(MainWindow::loggedIn == 0)
    {
        // wait...
        qDebug() << "Waiting...";
    }

    if (MainWindow::loggedIn == 1)
    {
        statusLabel_->setText("Connected!");
        loggedIn = 0;
        initializeMainPanel();
    }
    else if (MainWindow::loggedIn == -1)
    {
        statusLabel_->setText("Login failed!");
    }
    else if (MainWindow::loggedIn == -2)
    {
        statusLabel_->setText("Can't connect to server!");
    }
}

void MainWindow::createAccount()
{
    username_ = usernameInput_->text();
    password_ = passwordInput_->text();

    LogicThread *logicThread = new LogicThread(this, username_, password_, "Register");
    logicThread->start();

    while(MainWindow::registered == 0)
    {
        // wait...
        qDebug() << "Waiting...";
    }

    if (MainWindow::registered == 1)
    {
        qDebug() << "ok";
        registered = 0;
        //initializeMainPanel();
    }
    else if (MainWindow::registered == -1)
    {
        statusLabel_->setText("Registered failed!");
    }
}

void MainWindow::setStatusLabelText(QString newText)
{
    status_label_->setText(newText);
}

void MainWindow::addProblemToProblemsPanel()
{
    while(!MainWindow::conditionVariable)
    {
        // wait for problemsData to update
        // It's not the smartest way but it works...
    }

    if (problems.size() == 0)
    {
        statusLabel_->setText("Access denied!");
    }

    for (uint i = 0; i < problems.size(); i++)
    {
        QPushButton* auxProblemNameButton = new QPushButton(problems.at(i).getName() + "(" + QString::number(problems.at(i).getPoints()) + ")");
        auxProblemNameButton->setMaximumWidth(screenWidth / 6);

        auxProblemNameButton->setStyleSheet("QPushButton"
                                            "{"
                                                "background-color: #555871;"
                                                "color: white;"
                                                "border-radius: 10px;"
                                                "border: 2px solid black;"
                                                "margin-left: 50px"
                                            "}");

        auxProblemNameButton->setObjectName(QString::number(i));
        connect(auxProblemNameButton, SIGNAL(clicked()), this, SLOT(setProblemName()));

        bool categoryExists = false;
        for (Category* category : categories)
        {
            if (problems.at(i).getCategory() == category->getName())
            {
                category->addWidget(auxProblemNameButton);
                categoryExists = true;
            }
        }

        if (!categoryExists)
        {
            Category* newCategory = new Category(problems.at(i).getCategory());
            newCategory->addWidget(auxProblemNameButton);
            categories.push_back(newCategory);
        }
    }

    for (Category* category : categories)
    {
        category->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        scrollAreaLayout->addWidget(category);
    }
}

void MainWindow::setProblemName()
{
    QObject* senderObj = sender();
    problemID = static_cast<uint>(senderObj->objectName().toInt());

    problem_name_ = "Problems/C_" + problems.at(problemID).getCategory() + "/" + problems.at(problemID).getName();
    descriptionArea_->setText("Description: " + problems.at(problemID).getDescription());

    QString selectedProblemText = "Problem: " + problems.at(problemID).getName();
    status_label_->setText(selectedProblemText + "\nResult: " + MainWindow::score);
}

void MainWindow::getFileContent()
{
    file_as_source_ = true;

    QString file_path = QFileDialog::getOpenFileName(this,
            tr("Source file"), "",
            tr("CPP source (*.cpp);;All Files (*)"));

    qDebug() << "File: " << file_path;

    QFile source_code(file_path);

    if(!source_code.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Can't open the input data file!";
    }

    QTextStream source_code_stream(&source_code);

    code_ = source_code_stream.readAll();

    source_code_text_->setText(code_);

    for (int i = 0; i < code_.length(); i++)
    {
        if (code_[i] == '\n')
        {
            code_[i] = static_cast<char>(15);
        }
    }

    qDebug() << "Code: " << code_;
}

void MainWindow::sendCode()
{
    if (problem_name_.length() == 0)
    {
        status_label_->setText("Select a problem");
        return;
    }

    if (!file_as_source_)
    {
        code_ = source_code_text_->toPlainText();
    }

    file_as_source_ = false;

    for (int i = 0; i < code_.length(); i++)
    {
        if (code_[i] == '\n')
        {
            code_[i] = static_cast<char>(15);
        }
    }

    if (code_.contains("system"))
    {
        status_label_->setText("Your code can't contain the 'system' command!");
        return;
    }

    if (code_.contains("exec"))
    {
        status_label_->setText("Your code can't contain the 'exec' command!");
        return;
    }

    if (code_.length() == 0)
    {
        status_label_->setText("Code editor is empty!");
        return;
    }

    MainWindow::score = "-1";
    LogicThread* logicThread = new LogicThread(username_, password_, problem_name_, code_);
    logicThread->start();

    if (MainWindow::score == "-2")
    {
        status_label_->setText("Error! Please try again.");
    }

    while(MainWindow::score == "-1")
    {
        // Wait for score to update
        // Not the best method but it's working...
        qDebug() << MainWindow::score;
        QThread::msleep(100);
    }

    if (MainWindow::score == "-2")
    {
        status_label_->setText("There are no contributors available! Please try again later.");
    }
    else
    {
        QString selectedProblemText = "Problem: " + problems.at(problemID).getName();
        status_label_->setText(selectedProblemText + "\nResult: " + MainWindow::score);
    }
}

void MainWindow::onSourceCodeChanged()
{
    highlighter->highlightBlock(source_code_text_->document()->toPlainText());
}

MainWindow::~MainWindow()
{
}
