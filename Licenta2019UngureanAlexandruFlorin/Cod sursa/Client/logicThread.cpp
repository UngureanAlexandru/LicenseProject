#include "mainwindow.h"
#include "logicThread.h"

LogicThread::LogicThread(MainWindow* mainThreadPointer, QString username, QString password)
{
    this->mode = 0;
    this->mainThreadPointer = mainThreadPointer;
    this->username_ = username;
    this->password_ = password;
}

LogicThread::LogicThread(QString username, QString password, QString problem_name, QString code)
{
    this->code = code;
    this->problem_name = problem_name;
    this->mode = 1;

    username_ = username;
    password_ = password;
}

LogicThread::LogicThread(MainWindow* mainThreadPointer, QString username, QString password, QString action)
{
    this->mainThreadPointer = mainThreadPointer;
    username_ = username;
    password_ = password;

    if (action == "Login")
    {
        this->mode = 2;
    }
    else if (action == "Register")
    {
        this->mode = 3;
    }
}

void LogicThread::run()
{

    QFile configFile("config.txt");

    if(!configFile.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Can't open the config file!";
        return;
    }

    configFile.seek(0);

    QTextStream configStream(&configFile);

    configStream.seek(0);

    QString configData = configStream.readAll();

    configFile.close();

    QStringList configDataList = configData.split('\n');

    clientSocket = new QTcpSocket();
    clientSocket->connectToHost(configDataList[0].toStdString().c_str(), atoi(configDataList[1].toStdString().c_str()));

    if (!clientSocket->waitForConnected(CONNECTION_TIMEOUT))
    {
        qDebug() << "Can't connecto to server!";
        MainWindow::loggedIn = -2;
        return;
    }
    qDebug() << "Connected...";

    if (mode == 0)
    {
        MainWindow::conditionVariable = false;

        QByteArray type = "r";
        type.append('\n');

        sendData(type);

        qDebug() << "user name: " + username_;
        QByteArray userData = (username_ + "|" + password_).toStdString().c_str();
        sendData(userData);
        qDebug() << "user data: " + userData;

        if(!clientSocket->waitForReadyRead(READ_TIMEOUT))
        {
            // wait for some data to come
            qDebug() << "Read timeout! Line: " << __LINE__;
        }

        int number_of_problems = atoi(clientSocket->readLine());

        if (number_of_problems == -1)
        {
            MainWindow::conditionVariable = true;
            return;
        }

        MainWindow::problems.clear();

        for (int i = 0; i < number_of_problems; i++)
        {
            if(!clientSocket->waitForReadyRead(READ_TIMEOUT))
            {
                // wait for some data to come
                qDebug() << "Read timeout! Line: " << __LINE__;
                continue;
            }

            QByteArray aux = clientSocket->readLine();
            QStringList data = QString::fromStdString(aux.toStdString()).split('|');
            QString problem_name = data[0].split('/')[2];
            QString category = data[0].split('/')[1];
            category.remove(0, 2);

            QString description = data[1];
            QString points = data[2];

            Problem newProblem(problem_name, category, description, points.toInt());

            MainWindow::problems.push_back(newProblem);
            qDebug() << problem_name << " " << description << "-" << points;

        }
        MainWindow::conditionVariable = true;
    }
    else if (mode == 1)
    {
        do
        {
            QByteArray type = "s";
            type.append('\n');

            sendData(type);

            QByteArray userData = username_.toUtf8() + "|" + password_.toUtf8() + "|" + problem_name.toUtf8();
            QByteArray byte_code = code.toUtf8();

            sendData(userData);

            if(!clientSocket->waitForReadyRead(READ_TIMEOUT))
            {
                // wait for some data to come
                qDebug() << "Read timeout! Line: " << __LINE__;
            }

            QByteArray errorCode = clientSocket->readLine();
            qDebug() << "Eror code: " << errorCode;

            if (errorCode.length() == 0)
            {
                qDebug() << "Error! Line: " << __LINE__;
                break;
            }

            if (errorCode.at(0) == '1')
            {
                qDebug() << "Problem not found!";
                break;
            }
            else if (errorCode.at(0) == '2')
            {
                MainWindow::score = "-2";
                qDebug() << "There are no contributors available!";
                break;
            }

            sendData(byte_code);

            if(!clientSocket->waitForReadyRead(READ_TIMEOUT * 10)) // Wait maximum 50 seconds for the score
            {
                // wait for some data to come
                qDebug() << "Read timeout! Line: " << __LINE__;
            }
            QString score = clientSocket->readLine();

            for (int i = 0; i < score.length(); i++)
            {
                if (score[i] == 15)
                {
                    score[i] = '\n';
                }
            }
            qDebug() << "Score: " << score;
            MainWindow::score = score;
        }
        while(false);
    }
    else if (mode == 2)
    {
        do
        {
            QByteArray type = "l";
            type.append('\n');
            sendData(type);

            QByteArray userData = username_.toUtf8();
            userData += '|';
            userData += password_.toUtf8();
            sendData(userData);

            if(!clientSocket->waitForReadyRead(READ_TIMEOUT * 10)) // Wait maximum 50 seconds for the score
            {
                // wait for some data to come
                qDebug() << "Read timeout! Line: " << __LINE__;
            }
            QString result = clientSocket->readLine();
            qDebug() << result;
            if (result == "1")
            {
                MainWindow::loggedIn = 1;
            }
            else
            {
                MainWindow::loggedIn = -1;
            }
        }
        while(false);
    }
    else if (mode == 3)
        {
            do
            {
                QByteArray type = "n"; // new account
                type.append('\n');
                sendData(type);

                QByteArray userData = username_.toUtf8();
                userData += '|';
                userData += password_.toUtf8();
                sendData(userData);

                if(!clientSocket->waitForReadyRead(READ_TIMEOUT * 10)) // Wait maximum 50 seconds for the score
                {
                    // wait for some data to come
                    qDebug() << "Read timeout! Line: " << __LINE__;
                }
                QString result = clientSocket->readLine();
                qDebug() << result;
                if (result == "1")
                {
                    MainWindow::registered = 1;
                }
                else
                {
                    MainWindow::registered = -1;
                }
            }
            while(false);
        }
    clientSocket->close();
}

void LogicThread::sendData(QByteArray data)
{
    clientSocket->write(data);
    clientSocket->waitForBytesWritten();
    msleep(100);
}
