#ifndef CATEGORY_H
#define CATEGORY_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QDebug>

class Category : public QWidget
{
    Q_OBJECT

private slots:
    void buttonAction();

public:
    Category(QString name);
    void addWidget(QWidget* problem);
    QString getName();

private:
    void expand();
    void collapse();

    QString name;
    std::vector<QWidget*> problems;
    int initialHeight = 50;
    bool state = false;

    QWidget* rootWidget;
    QPushButton* expandButton;
    QVBoxLayout* rootLayout;
    QVBoxLayout* widgetsLayout;
};

#endif // CATEGORY_H
