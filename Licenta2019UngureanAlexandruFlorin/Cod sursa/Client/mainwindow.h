#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QLineEdit>
#include <QScrollArea>

#include "problem.h"
#include "category.h"
#include "codeHighlighter.h"

class LogicThread;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    static std::vector<Problem> problems;
    static QString score;
    static bool conditionVariable;
    static int loggedIn;
    static int registered;

    ~MainWindow();

private slots:
    void getFileContent();
    void sendCode();
    void setProblemName();
    void login();
    void createAccount();
    void onSourceCodeChanged();

private:
    void init();
    void addProblemToProblemsPanel();
    void setStatusLabelText(QString newText);

    void initializeMainPanel();
    void initializeLoginOrRegisterPanel();

    QWidget* mainPanel_;
    QWidget* usernamePanel_;
    QWidget* passwordPanel_;

    QHBoxLayout* root_layout_;
    QVBoxLayout* layout_;
    QPushButton* load_file_button_;
    QPushButton* send_code_button_;
    QTextEdit* status_label_;
    QTextEdit* source_code_text_;
    CodeHighlighter *highlighter;

    QWidget* loginOrRegisterPanel_;
    QVBoxLayout* loginOrRegisterVLayout_;

    QHBoxLayout* usernameLayout_;
    QHBoxLayout* passwordLayout_;

    QLabel* loginOrRegisterTitle_;

    QLabel* usernameLabel_;
    QLineEdit* usernameInput_;
    QLabel* passwordLabel_;
    QLineEdit* passwordInput_;
    QPushButton* loginButton_;
    QPushButton* registerButton_;
    QLabel* statusLabel_;

    QString code_;
    QString problem_name_;

    QString username_;
    QString password_;

    bool file_as_source_;

    bool mainPanelInitialized_;
    bool loginOrRegisterPanelInitialized_;

    QTextEdit* descriptionArea_;
    QScrollArea* problemsScrollArea_;
    QWidget* scrollAreaRootWidget;
    QVBoxLayout* scrollAreaLayout;
    std::vector<Category*> categories;

    int screenWidth;
    int screenHeight;
    uint problemID;
};

#endif // MAINWINDOW_H
