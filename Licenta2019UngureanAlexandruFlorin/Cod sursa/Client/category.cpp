#include "category.h"

Category::Category(QString name)
{
    this->name = name;

    resize(width(), initialHeight);

    setStyleSheet("QPushButton#Category"
                   "{"
                        "background-color: #555871;"
                        "color: white;"
                        "border: 2px solid black;"
                        "padding: 5px"
                    "}");

    rootWidget = new QWidget();

    expandButton = new QPushButton(this);
    expandButton->setObjectName("Category");

    widgetsLayout = new QVBoxLayout(this);
    rootLayout = new QVBoxLayout(this);

    rootWidget->setLayout(rootLayout);
    rootLayout->addWidget(expandButton);
    rootLayout->addLayout(widgetsLayout);

    //widgetsLayout->setContentsMargins(50, 0, 0, 0);

    expandButton->setText(name);

    this->layout()->addWidget(rootWidget);

    connect(expandButton, SIGNAL(clicked(bool)), this, SLOT(buttonAction()));
}

void Category::addWidget(QWidget* widget)
{
    widget->setVisible(false);
    problems.push_back(widget);
    widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    widgetsLayout->addWidget(widget);
}

QString Category::getName()
{
    return name;
}

void Category::buttonAction()
{
    if (!state)
    {
        state = true;
        expand();
    }
    else
    {
        state = false;
        collapse();
    }
}

void Category::expand()
{
    resize(width(), initialHeight * static_cast<int>(problems.size() + 1));

    for (QWidget* problem : problems)
    {
        problem->setVisible(true);
    }
}

void Category::collapse()
{
    resize(width(), initialHeight);

    for (QWidget* problem : problems)
    {
        problem->setVisible(false);
    }
}
