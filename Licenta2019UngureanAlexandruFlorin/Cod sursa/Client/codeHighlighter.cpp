#include "codeHighlighter.h"

#include <QRegularExpression>

CodeHighlighter::CodeHighlighter(QTextDocument *document) : QSyntaxHighlighter(document)
{
}

void CodeHighlighter::highlightBlock(const QString &text)
{
    QTextCharFormat includeFormat;
    includeFormat.setFontWeight(QFont::Bold);
    includeFormat.setForeground(QColor(65, 126, 224));

    QTextCharFormat soroundedStringFormat;
    soroundedStringFormat.setFontWeight(QFont::Bold);
    soroundedStringFormat.setForeground(QColor(176, 74, 239));

    QTextCharFormat typesFormat;
    typesFormat.setFontWeight(QFont::Bold);
    typesFormat.setForeground(QColor(76, 191, 91));

    QTextCharFormat decimalConstantsFormat;
    decimalConstantsFormat.setFontWeight(QFont::Bold);
    decimalConstantsFormat.setForeground(QColor(116, 143, 219));

    QRegularExpression keywords("#include|(?<!\\w)if(?!\\w)|(?<!\\w)else(?!\\w)|(?<!\\w)while(?!\\w)|(?<!\\w)for(?!\\w)|(?<!\\w)do(?!\\w)|using(?!\\w)|(?<!\\w)namespace(?!\\w)|(?<!\\w)return(?!\\w)|(?<!\\w)printf(?!\\w)|(?<!\\w)continue(?!\\w)|(?<!\\w)break(?!\\w)");
    QRegularExpression soroundedString("\".*\"|<.*>");
    QRegularExpression types("(?<!\\w)int(?!\\w)|(?<!\\w)string(?!\\w)|(?<!\\w)bool(?!\\w)|(?<!\\w)char(?!\\w)|(?<!\\w)long(?!\\w)|(?<!\\w)float(?!\\w)|(?<!\\w)ifstream(?!\\w)|(?<!\\w)ofstream(?!\\w)");
    QRegularExpression decimalConstants("(?<!\\w)[0-9]*(?!\\w)");

    QRegularExpressionMatchIterator i0 = keywords.globalMatch(text);
    QRegularExpressionMatchIterator i1 = soroundedString.globalMatch(text);
    QRegularExpressionMatchIterator i2 = types.globalMatch(text);
    QRegularExpressionMatchIterator i3 = decimalConstants.globalMatch(text);

    while (i0.hasNext())
    {
        QRegularExpressionMatch match = i0.next();
        setFormat(match.capturedStart(), match.capturedLength(), includeFormat);
    }

    while (i1.hasNext())
    {
        QRegularExpressionMatch match = i1.next();
        setFormat(match.capturedStart(), match.capturedLength(), soroundedStringFormat);
    }

    while (i2.hasNext())
    {
        QRegularExpressionMatch match = i2.next();
        setFormat(match.capturedStart(), match.capturedLength(), typesFormat);
    }

    while (i3.hasNext())
    {
        QRegularExpressionMatch match = i3.next();
        setFormat(match.capturedStart(), match.capturedLength(), decimalConstantsFormat);
    }
}
