#ifndef PROBLEM_H
#define PROBLEM_H

#include <QString>

class Problem
{
public:
    Problem(QString name, QString category, QString description, int points);
    void updateScore(int points);
    QString getName();
    QString getCategory();
    QString getDescription();
    int getPoints();

private:
    QString name;
    QString category;
    QString description;
    int points;
};

#endif // PROBLEM_H
