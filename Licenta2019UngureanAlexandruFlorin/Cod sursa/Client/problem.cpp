#include "problem.h"

Problem::Problem(QString name, QString category, QString description, int points)
{
    this->name = name;
    this->category = category;
    this->description = description;
    this->points = points;
}

void Problem::updateScore(int points)
{
    this->points = points;
}

QString Problem::getName()
{
    return name;
}

QString Problem::getCategory()
{
    return category;
}

QString Problem::getDescription()
{
    return description;
}

int Problem::getPoints()
{
    return points;
}
