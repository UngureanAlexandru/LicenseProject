#ifndef CODEHIGHLIGHTER_H
#define CODEHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextDocument>
#include <QDebug>

class CodeHighlighter: public QSyntaxHighlighter
{
    Q_OBJECT

public:
    CodeHighlighter(QTextDocument *document);
    void highlightBlock(const QString &text);
};

#endif // CODEHIGHLIGHTER_H
