#include <QCoreApplication>

#include <server.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int port;
    if (argc < 2)
    {
        port = 3000;
    }
    else
    {
        port = atoi(argv[1]);
    }
    Server *server = new Server(port);

    return a.exec();
}
