#ifndef SERVER_H
#define SERVER_H

#include <QTcpSocket>
#include <QTcpServer>
#include <QThread>
#include <QObject>
#include <QMutex>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>

#include <vector>

#include "problem.h"

#define READ_TIMEOUT 10000

class ClientThread;

class Server : public QTcpServer
{
    Q_OBJECT

public:
    Server(int newPort, QObject *parent = 0);
    QTcpServer* getServerSocket();
    std::vector<std::pair<QTcpSocket*, bool>> getContributors();

    void addContributor(QTcpSocket* newContributor);
    void removeContributor(uint contributorID);
    void changeContributorState(int contributorIndex, bool state);

    void addBusyProblem(QString problemName);
    void removeBusyProblem(QString problemName);
    bool checkIfBusyProblem(QString problemName);

    std::vector<Problem> getProblems();

    static QMutex mutex;

private:

    void init();
    void initializeProblemsVector(QString rootPath);
    void removeNewLines(QString& text);

    QTcpServer* serverSocket;
    std::vector<std::pair<QTcpSocket*, bool>> contributors;

    QSqlDatabase db;
    int port;
    std::vector<QString> busyProblems;
    std::vector<Problem> problems;

protected:
        void incomingConnection(qintptr socketDescriptor) override;
};

#endif // SERVER_H
