#include "problem.h"

Problem::Problem(QString name, QString category, QString description)
{
    this->name = name;
    this->category = category;
    this->description = description;
}

QString Problem::getName()
{
    return name;
}

QString Problem::getCategory()
{
    return category;
}

QString Problem::getDescription()
{
    return description;
}
