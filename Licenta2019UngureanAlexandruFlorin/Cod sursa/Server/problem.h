#ifndef PROBLEM_H
#define PROBLEM_H

#include <QString>

class Problem
{
public:
    Problem(QString name, QString category, QString description);
    void updateScore(int points);
    QString getName();
    QString getCategory();
    QString getDescription();

private:
    QString name;
    QString category;
    QString description;
};

#endif // PROBLEM_H
