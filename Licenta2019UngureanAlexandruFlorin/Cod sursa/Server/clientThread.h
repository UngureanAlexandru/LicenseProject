#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <QtNetwork/QTcpSocket>
#include <QFile>
#include <QDir>
#include <QThread>
#include <QMutex>

#include "problem.h"

#define READ_TIMEOUT 5000

class Server;

class ClientThread : public QThread
{
public:
    ClientThread(Server* serverPointer, qintptr socketDescriptor);
    static QMutex* mutex;

public:
    void run();

private:
    bool login(QString username, QString password);
    void getHardwareInfo();
    void sendData(QTcpSocket *socket, QByteArray data);

    QTcpSocket* clientSocket;
    std::vector<std::pair<QTcpSocket*, bool>> contributors;
    Server* serverPointer;
    qintptr socketDescriptor;

    QString problemName;
    QString code;
    QString inputData;
    QString outputData;

    int ram;
    int cpu;
    int selectedContributor;
    std::vector<Problem> problems;
};

#endif // CLIENTTHREAD_H
