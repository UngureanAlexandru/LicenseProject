#include "clientThread.h"
#include "server.h"

#include "/home/alexandru/Applications/boost_1_70_0/boost/property_tree/ptree.hpp"
#include "/home/alexandru/Applications/boost_1_70_0/boost/property_tree/xml_parser.hpp"
#include "/home/alexandru/Applications/boost_1_70_0/boost/foreach.hpp"
#include <string>
#include <set>
#include <exception>
#include <iostream>

namespace pt = boost::property_tree;

QMutex *ClientThread::mutex = new QMutex();

ClientThread::ClientThread(Server* serverPointer, qintptr socketDescriptor)
{
    this->serverPointer = serverPointer;
    this->socketDescriptor = socketDescriptor;

    cpu = 101;
    ram = 101;
    selectedContributor = -1;

    problems = serverPointer->getProblems();
}

void ClientThread::run()
{
    do
    {
        clientSocket = new QTcpSocket();

        if (!clientSocket->setSocketDescriptor(socketDescriptor))
        {
            qDebug() << "Can't set socket descriptor! Line: " << __LINE__;
            break;
        }

        if (!clientSocket)
        {
            qDebug("Null socket!");
            break;
        }

        while(!clientSocket->waitForReadyRead(READ_TIMEOUT))
        {
            // wait for some data to come
            qDebug() << "Read timeout! Line: " << __LINE__;
        }

        QByteArray type = clientSocket->readLine();
        qDebug() << "Type: " << type;

        if (type.at(0) == 'c')
        {
            qDebug() << "New contributor!";
            serverPointer->addContributor(clientSocket);
            break;
        }

        if (type.at(0) == 'r')
        {
            qDebug() << "Request for problems list!";

            QSqlQuery query;
            QByteArray result = "-1";

            while(!clientSocket->waitForReadyRead(READ_TIMEOUT))
            {
                // wait for some data to come
                qDebug() << "Read timeout! Line: " << __LINE__;
            }

            QString userData = clientSocket->readLine();
            qDebug() << "user data: " + userData;
            QStringList userDataList = userData.split("|");
            QString username = userDataList[0];
            QString password = userDataList[1];
            qDebug() << "before List username: " + username;

            query.prepare("select * from users where username = :name and password = :pass;");
            query.bindValue(":name", username);
            query.bindValue(":pass", password);

            qDebug() << "List username: " + username;

            if (!query.exec() || !query.next())
            {
                sendData(clientSocket, "-1");
                break;
            }

            QString problemsLength = QString::number(problems.size());

            sendData(clientSocket, problemsLength.toStdString().c_str());

            for (int i = 0; i < problems.size(); i++)
            {
                QSqlQuery query;
                query.prepare("select points from " + username + " where problemName = :problemName;");
                query.bindValue(":problemName", problems.at(i).getCategory() + "/" + problems[i].getName());

                if(!query.exec())
                {
                    qDebug() << "Failed to get points!";
                    qDebug() << username + " - " + problems.at(i).getCategory() + "/" + problems[i].getName();
                }

                int maxPoints = 0;

                while (query.next())
                {
                    if (atoi(query.value(0).toString().toStdString().c_str()) > maxPoints)
                    {
                        maxPoints = atoi(query.value(0).toString().toStdString().c_str());
                    }
                }

                QString aux = problems[i].getCategory() + "/" + problems[i].getName() + "|" + problems[i].getDescription() + "|" + QString::number(maxPoints);
                qDebug() << "Problem: " << aux;
                sendData(clientSocket, aux.toStdString().c_str());
            }

            break;
        }

        if (type.at(0) == 'l')
        {
            qDebug() << "Login!";

            QString result = "-1";

            if(!clientSocket->waitForReadyRead(READ_TIMEOUT))
            {
                // wait for some data to come
                qDebug() << "Read timeout! Line: " << __LINE__;
            }
            msleep(50);

            QString userData = clientSocket->readLine();
            QStringList userDataList = userData.split('|');
            QString username = userDataList[0];
            QString password = userDataList[1];
            qDebug() << "User: " + username + " - Pass: " + password;

            QSqlQuery query;

            query.prepare("select 1 from users where username = :name and password = :pass");
            query.bindValue(":name", username);
            query.bindValue(":pass", password);
            query.exec();

            if (query.next())
            {
                result = "1";
            }
            qDebug() << "Login result: " + result;
            sendData(clientSocket, result.toStdString().c_str());

            break;
        }

        if (type.at(0) == 'n') // new account
        {
            qDebug() << "New account!";

            QString result = "-1";

            if(!clientSocket->waitForReadyRead(READ_TIMEOUT))
            {
                // wait for some data to come
                qDebug() << "Read timeout! Line: " << __LINE__;
            }
            msleep(50);

            QString userData = clientSocket->readLine();
            QStringList userDataList = userData.split('|');
            QString username = userDataList[0];
            QString password = userDataList[1];

            bool validUsername = true;
            for (int i = 0; i < username.length(); i++)
            {
                if (username.at(i) < 65 || username.at(i) > 122 || ((username.at(i) > 90) && (username.at(i) < 97)))
                {
                    qDebug() << result;
                    sendData(clientSocket, result.toStdString().c_str());
                    validUsername = false;
                }
            }

            if(!validUsername)
            {
                break;
            }

            QSqlQuery query;

            query.prepare("insert into users (username, password) values (:name, :pass)");
            query.bindValue(":name", username);
            query.bindValue(":pass", password);

            if (query.exec())
            {
                result = "1";
            }

            QSqlQuery query2;
            query2.prepare("create table " + username + " (id int(6) unsigned auto_increment primary key, problemName varchar(50) not null, points int(3) not null);");
            query2.bindValue(":name", username);

            if (query2.exec())
            {
                result = "1";
            }

            qDebug() << result;
            sendData(clientSocket, result.toStdString().c_str());

            break;
        }

        if (type.at(0) != 's')
        {
            qDebug() << "Error! Unknown type!";
            break;
        }
        qDebug() << "New student!";

        this->contributors = serverPointer->getContributors();

        if(!clientSocket->waitForReadyRead(READ_TIMEOUT))
        {
            // wait for some data to come
            qDebug() << "Read timeout! Line: " << __LINE__;
        }
        msleep(50);

        QString userData = clientSocket->readLine();
        QStringList userDataList = userData.split("|");

        QString username = userDataList[0];
        QString password = userDataList[1];

        QByteArray errorCode; // 0 - dir exists, 1 - dir does not exists, 2 - no contributors

        QSqlQuery query;
        errorCode = "-1";

        query.prepare("select * from users where username = :name and password = :pass;");
        query.bindValue(":name", username);
        query.bindValue(":pass", password);

        if (query.exec())
        {
            errorCode = "0";
        }

        if (errorCode == "-1")
        {
            sendData(clientSocket, errorCode);
            break;
        }

        problemName = userDataList[2];
        qDebug() << "Problem name: " << problemName;

        if(QDir(problemName).exists())
        {
            errorCode = "0";
        }
        else
        {
            errorCode = "1";
        }

        if (contributors.size() == 0)
        {
            errorCode = "2";
        }

        sendData(clientSocket, errorCode);

        if (errorCode != "0")
        {
            qDebug() << "Error: " << errorCode;
            break;
        }

        if(!clientSocket->waitForReadyRead(READ_TIMEOUT))
        {
            // wait for some data to come
            qDebug() << "Read timeout! Line: " << __LINE__;
            break;
        }

        code = clientSocket->readLine();
        qDebug() << "Code " << code;

        ClientThread::mutex->lock();
        getHardwareInfo();

        if (cpu == -1 && ram == -1)
        {
            qDebug() << "Error at contributor tests split! Line: " << __LINE__;

            int nullScore = 0;
            QByteArray errorResponseBytes = QString::number(nullScore).toUtf8();

            sendData(clientSocket, errorResponseBytes);

            continue;
        }

        for (int i = 0; i < contributors.size(); i++)
        {
            if (i == selectedContributor)
            {
                continue;
            }

            QByteArray refuseMessage = "0";
            refuseMessage.append('\n');

            sendData(contributors.at(i).first, refuseMessage);
        }
        ClientThread::mutex->unlock();

        if (selectedContributor == -1 || static_cast<uint>(selectedContributor) > contributors.size())
        {
            qDebug() << "Wrong selectedContributor value! Value: " <<selectedContributor << ". Line: " << __LINE__;

            QByteArray contributorError = "Contributors error!";
            sendData(clientSocket, contributorError);

            break;
        }

        serverPointer->changeContributorState(selectedContributor, true);
        contributors.at(static_cast<uint>(selectedContributor)).second = true;

        qDebug() << selectedContributor << ": CPU: " << cpu << " RAM: " << ram << " Selected contributor: " << selectedContributor;

        QString inputDataPath = problemName + "/input.xml";

        QFile inputDataFile(inputDataPath);

        if(!inputDataFile.open(QFile::ReadOnly | QFile::Text))
        {
            qDebug() << "Can't open the input data file!";
            break;
        }

        inputDataFile.seek(0);

        QTextStream inputDataStream(&inputDataFile);

        inputDataStream.seek(0);

        inputData = inputDataStream.readAll();

        inputDataFile.close();

        for (int i = 0; i < inputData.length(); i++)
        {
            if (inputData[i] == '\n')
            {
                inputData[i] = 15;
            }
        }

        outputData = outputData.trimmed();

        qDebug() << selectedContributor << ": Input data" << inputData.toStdString().c_str();
        qDebug() << selectedContributor << ": Output data" << outputData.toStdString().c_str();
        qDebug() << selectedContributor << ": Size: " << contributors.size() << " id: " << selectedContributor;
        code += '\n';

        sendData(contributors.at(selectedContributor).first, code.toUtf8());

        sendData(contributors.at(selectedContributor).first, inputData.toStdString().c_str());

        if(!contributors.at(selectedContributor).first->waitForReadyRead(READ_TIMEOUT))
        {
            // wait for some data to come
            qDebug() << "Read timeout! Line: " << __LINE__;
        }

        QString contributorOutputData = contributors.at(selectedContributor).first->readLine();
        serverPointer->changeContributorState(selectedContributor, false);
        contributors.at(selectedContributor).second = false;

        for (int i = 0; i < contributorOutputData.size(); i++)
        {
            if (contributorOutputData[i] == 15)
            {
                contributorOutputData[i] = '\n';
            }
        }

        qDebug() << selectedContributor << ": Contributor output: " << contributorOutputData;

        if (contributorOutputData.length() == 0)
        {
            qDebug() << "Invalid output contributor size! Line: " << __LINE__;

            QByteArray scoreBytes = QString::number(0).toUtf8();

            sendData(clientSocket, scoreBytes);

            break;
        }

        int score = 0;
        QString outputDataPath = problemName + "/output.xml";
        pt::ptree outputTree;
        pt::read_xml(outputDataPath.toStdString(), outputTree);

        std::vector<std::string> outputTests;
        BOOST_FOREACH(pt::ptree::value_type &v, outputTree.get_child("root"))
        {
            outputTests.push_back(v.second.data());
            qDebug() << QString::fromStdString(v.second.data());
        }

        qDebug() << selectedContributor << ": Contributor tests: " << contributorOutputData;

        pt::ptree contributorOutputTree;
        std::stringstream ss;
        ss << contributorOutputData.toStdString();

        try
        {
            pt::read_xml(ss, contributorOutputTree);
        }
        catch (int e)
        {
            qDebug() << "Invalid XML! Line: " << __LINE__;
            QByteArray error = "XML error!";
            sendData(clientSocket, error);
            break;
        }

        int outputResultsID = 0;
        bool errorFound = false;
        QByteArray errorMessage;

        BOOST_FOREACH(pt::ptree::value_type &v, contributorOutputTree.get_child("root"))
        {
            if (outputTests.size() > outputResultsID)
            {
                qDebug() << "Test: " << outputResultsID << ": " << QString::fromStdString(v.second.data()) << " " << QString::fromStdString(outputTests.at(outputResultsID));

                if (v.second.data() == "Error")
                {
                    errorFound = true;
                }
                else if (errorFound)
                {
                    errorMessage = v.second.data().c_str();

                    for (int i = 0; i < errorMessage.length(); i++)
                    {
                        if (errorMessage[i] == '\n')
                        {
                            errorMessage[i] = 15;
                        }
                    }
                    break;
                }

                if (outputTests.at(outputResultsID) == v.second.data())
                {
                    score += (100 / outputTests.size());
                }
            }
            outputResultsID++;
        }

        if (errorFound)
        {
            sendData(clientSocket, errorMessage);
            clientSocket->close();
            break;
        }

        if (score == 99)
        {
            score = 100;
        }

        qDebug() << "Username:" << username << ", problem name: " << problemName << ", score: " << score;

        QSqlQuery insertQuery;
        insertQuery.prepare("insert into " + username + " (problemName, points) values (:problem, :score);");

        insertQuery.bindValue(":problem", problemName);
        insertQuery.bindValue(":score", score);

        if (!insertQuery.exec())
        {
            qDebug() << "Error at query! Line: " << __LINE__;
            qDebug() << insertQuery.lastQuery();
        }

        QByteArray scoreBytes = QString::number(score).toUtf8();

        sendData(clientSocket, scoreBytes);

        qDebug() << "Score: " << score;

        clientSocket->close();
        qDebug() << "Finished thread!\n";
    }
    while(false);
}

void ClientThread::getHardwareInfo()
{
    selectedContributor = -1;
    cpu = 101;
    ram = 101;

    QByteArray message = "1"; // get current ram and cpu load
    message.append('\n');

    QByteArray statusRequest = "2"; // get contributor status
    statusRequest.append('\n');

    qDebug() << "Contributor size: " << contributors.size();

    for (int i = 0; i < contributors.size(); i++)
    {

        if (contributors.at(i).first->state() != QTcpSocket::ConnectedState)
        {
            serverPointer->removeContributor(i);
            contributors.erase(contributors.begin() + i);
            i = 0;
            continue;
        }

        qDebug() << "Before State of " << i << ": " << contributors.at(i).second;
        if (contributors.at(i).second == true)
        {
            continue;
        }

        sendData(contributors.at(i).first, statusRequest);

        if(!contributors.at(i).first->waitForReadyRead(READ_TIMEOUT))
        {
            // wait for some data to come
            qDebug() << "Read timeout! Line: " << __LINE__;

            i = 0;
            continue;
        }

        QString contributorStatus = contributors.at(i).first->readLine();
        qDebug() << "Contributor status: " << contributorStatus;

        if (contributorStatus.at(0) != '1')
        {
            continue;
        }

        sendData(contributors.at(i).first, message);

        if(!contributors.at(i).first->waitForReadyRead(READ_TIMEOUT))
        {
            // wait for some data to come
            qDebug() << "Read timeout! Line: " << __LINE__;
            continue;
        }

        QString hardwareInfo = contributors.at(i).first->readLine();
        qDebug() << "Raw hardware info: " << hardwareInfo;

        QStringList hardwareInfoList = hardwareInfo.split('|');

        if (hardwareInfoList.length() != 2)
        {
            continue;
        }

        qDebug() << "Hardware info list: " << hardwareInfoList;
        qDebug() << "Hardware info list length: " << hardwareInfoList.length();

        QByteArray cpuAux = hardwareInfoList.at(0).toUtf8();
        int cpuAuxInt = atoi(cpuAux);
        qDebug() << "Aux cpu: " << cpuAuxInt;

        QByteArray ramAux = hardwareInfoList.at(1).toUtf8();
        int ramAuxInt = atoi(ramAux);
        qDebug() << "Aux ram: " << ramAuxInt;

        if (
                (cpuAuxInt < cpu && ramAuxInt < ram) ||
                (cpuAuxInt <= cpu && ramAuxInt < ram) ||
                (cpuAuxInt < cpu && ramAuxInt <= ram)
           )
        {
            selectedContributor = i;
            cpu = cpuAuxInt;
            ram = ramAuxInt;
        }
        qDebug() << "After State of " << i << ": " << contributors.at(i).second;
    }
    qDebug() << "Contributor size: " << contributors.size();
}

void ClientThread::sendData(QTcpSocket *socket, QByteArray data)
{
    socket->write(data);
    socket->waitForBytesWritten();
    msleep(150);
}
