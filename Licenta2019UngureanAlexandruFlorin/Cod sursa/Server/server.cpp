#include <QDir>

#include "server.h"
#include "clientThread.h"

Server::Server(int newPort, QObject *parent) : QTcpServer (parent)
{
    port = newPort;
    initializeProblemsVector("Problems");
    init();
}

void Server::init()
{
    db = QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("localhost");
    db.setDatabaseName("license");
    db.setUserName("root");
    db.setPassword("Darkness0");

    if(db.open())
    {
        printf("Database opened!\n");
    }
    else
    {
        printf("Error! Can't open database!\n");
        return;
    }

    if (this->listen(QHostAddress::Any, port))
    {
        qDebug() << "Listening...";
    }
    else
    {
        qDebug() << "Can't listen...";
    }
}

void Server::incomingConnection(qintptr socketDescriptor)
{
    QThread* thread = new ClientThread(this, socketDescriptor);
    thread->start();
}

std::vector<std::pair<QTcpSocket*, bool>> Server::getContributors()
{
    return Server::contributors;
}

void Server::addContributor(QTcpSocket* newContributor)
{
    std::pair<QTcpSocket*, bool> newPair;
    newPair.first = newContributor;
    newPair.second = false;

    Server::contributors.push_back(newPair);
}

void Server::removeContributor(uint contributorID)
{
    contributors.erase(contributors.begin() + contributorID);
}

QTcpServer* Server::getServerSocket()
{
    return serverSocket;
}

void Server::changeContributorState(int contributorIndex, bool state)
{
    Server::contributors.at(contributorIndex).second = state;
}

void Server::initializeProblemsVector(QString rootPath)
{
    QDir root(rootPath);
    QStringList entries = root.entryList(QDir::Dirs);

    for(QString entry : entries)
    {
        if (entry == "." || entry == "..")
        {
            continue;
        }

        qDebug() << rootPath + "/" + entry;
        QString entryPath = rootPath + "/" + entry;

        if (entry.startsWith("C_"))
        {
            initializeProblemsVector(rootPath + "/" + entry);
        }
        else
        {
            QFile descriptionFile (entryPath + "/description.txt");

            if (!descriptionFile.open(QFile::ReadOnly | QFile::Text))
            {
                qDebug() << "Can't open the description file! Line: " << __LINE__;
                continue;
            }
            QTextStream descriptionText (&descriptionFile);
            QString descripion = descriptionText.readAll();

            removeNewLines(descripion);

            Problem newProblem(entry, rootPath, descripion);
            problems.push_back(newProblem);
            descriptionFile.close();
        }
    }
}

std::vector<Problem> Server::getProblems()
{
    return problems;
}

void Server::removeNewLines(QString& text)
{
    for (uint i = 0; i < text.size(); i++)
    {
        if (text[i] == '\n')
        {
            text[i] = ' ';
        }
    }
}
