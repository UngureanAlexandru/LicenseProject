#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    mainWidget = new QWidget();
    layout = new QVBoxLayout();
    onOffButton = new QPushButton();
    statusLabel = new QLabel();

    connect(onOffButton, SIGNAL(clicked()), this, SLOT(onOffFunction()));

    setCentralWidget(mainWidget);

    layout->addWidget(onOffButton);
    layout->addWidget(statusLabel);
    mainWidget->setLayout(layout);

    LogicThread::status = "";

    logicThread = new LogicThread();
    logicThread->start();

    while(LogicThread::status == "")
    {
        //wait
    }

    statusLabel->setText(LogicThread::status);

    onOffButton->setText("Turn on");
    state = 0;
}

void MainWindow::onOffFunction()
{
    logicThread->changeState();
    if (!state)
    {
        state = 1;
        onOffButton->setText("Turn off");
    }
    else if (state)
    {
        state = 0;
        onOffButton->setText("Turn on");
    }

}

MainWindow::~MainWindow()
{
}
