#include "LogicThread.h"

#include <string>
#include <set>
#include <exception>
#include <iostream>

#ifdef __linux__
#include "/home/alexandru/Applications/boost_1_70_0/boost/property_tree/ptree.hpp"
#include "/home/alexandru/Applications/boost_1_70_0/boost/property_tree/xml_parser.hpp"
#include "/home/alexandru/Applications/boost_1_70_0/boost/foreach.hpp"
#elif _WIN32
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#else
qDebug() << "What OS is this?";
#endif

namespace pt = boost::property_tree;

QString LogicThread::status = "";

LogicThread::LogicThread()
{}

void LogicThread::run()
{
    init();
}

void LogicThread::init()
{
    do
    {
        active = 0;

        QFile configFile("config.txt");

        if(!configFile.open(QFile::ReadOnly | QFile::Text))
        {
            LogicThread::status = "Can't open the config file!";
            qDebug() << status;
            return;
        }

        configFile.seek(0);

        QTextStream configStream(&configFile);
        configStream.seek(0);

        QString configData = configStream.readAll();
        configFile.close();

        QStringList configDataList = configData.split('\n');

        contributorSocket = new QTcpSocket();
        contributorSocket->connectToHost(configDataList[0].toStdString().c_str(), atoi(configDataList[1].toStdString().c_str()));

        if (!contributorSocket->waitForConnected())
        {
            LogicThread::status = "Can't connect to server!";
            qDebug() << LogicThread::status;
            break;
        }
        else
        {
            LogicThread::status = "Connected to server!";
            qDebug() << LogicThread::status;
        }

        QByteArray type = "c";
        type.append('\n');

        contributorSocket->write(type);
        contributorSocket->flush();

        info = new HardwareInfoThread();
        info->start();

        loop();
    }
    while(false);
}

void LogicThread::changeState()
{
    active = !active;
}

void LogicThread::loop()
{

    while(true)
    {
        contributorSocket->readAll();
        msleep(100);
        qDebug() << "Active: " << active;

        if(!contributorSocket->waitForReadyRead(READ_TIMEOUT))
        {
            // wait until some data comes
            qDebug() << "Read timeout!  Line: " << __LINE__;
            continue;
        }

        QByteArray requestStatus = contributorSocket->readLine();
        qDebug() << "Request status message: " << requestStatus.at(0);

        if (requestStatus.at(0) != '2')
        {
            qDebug() << "Error! Wrong message! Line: " << __LINE__;
            qDebug() << "Received message: " << requestStatus.at(0);
            continue;
        }

        QByteArray status;
        status = "0";
        status.append('\n');

        if (active)
        {
            status = "1";
            status.append('\n');
        }

        sendData(contributorSocket, status);
        qDebug() << "Status sent: " << status;

        if (!active)
        {
            continue;
        }

        if(!contributorSocket->waitForReadyRead(READ_TIMEOUT))
        {
            // wait until some data comes
            qDebug() << "Read timeout!  Line: " << __LINE__;
            continue;
        }

        QByteArray message = contributorSocket->readLine();
        qDebug() << "Request message: " << message.at(0);

        if (message.at(0) != '1')
        {
            qDebug() << "Error! Wrong message! Line: " << __LINE__;
            qDebug() << "Received message: " << message.at(0);
            continue;
        }

        getHardwareInfo();

        QString cpuString = QString::number(cpu);
        QString ramString = QString::number(ram);
        QString result = cpuString + "|" + ramString;
        qDebug() << "CPU and RAM result: " << result;

        sendData(contributorSocket, result.toStdString().c_str());

        if(!contributorSocket->waitForReadyRead(READ_TIMEOUT))
        {
            qDebug() << "Read timeout!  Line: " << __LINE__;
            continue;
        }

        code = contributorSocket->readLine();

        if (code.at(0) == '0')
        {
            qDebug() << "Not the chosen one!";
            continue;
        }

        for (int i = 0; i < code.length(); i++)
        {
            if (code[i] == static_cast<char>(15))
            {
                code[i] = '\n';
            }
        }

        qDebug() << "Code: " << code;

        if(!contributorSocket->waitForReadyRead(READ_TIMEOUT))
        {
            // wait until some data comes
            qDebug() << "Read timeout! Line: " << __LINE__;
            continue;
        }

        inputData = contributorSocket->readLine();

        for (int i = 0; i < inputData.length(); i++)
        {
            if (inputData[i] == static_cast<char>(15))
            {
                inputData[i] = '\n';
            }
        }

        qDebug() << "Input data: " << inputData;

        runCode();

        sendData(contributorSocket, outputData);
    }

}

void LogicThread::getHardwareInfo()
{
    cpu = info->getCPU();
    ram = info->getRAM();

    qDebug() << "CPU: " << cpu << " RAM: " << ram;
}

QByteArray LogicThread::intToByteArray(int input)
{
    QByteArray result;

    for (uint i = 0; i < sizeof(int); i++)
    {
        result.push_back(input >> (sizeof(int) - 1 - i) & 0xFF);
    }
    return result;
}

void LogicThread::runCode()
{
    do
    {
        QFile file ("code");
        file.remove();

        QFile file2 ("code.c");
        file2.remove();

        QFile file3 ("input.txt");
        file3.remove();

        QFile file4 ("output.txt");
        file4.remove();

        QFile file5 ("inputXml.xml");
        file5.remove();

        QFile file6 ("outputXml.xml");
        file6.remove();

        QFile codeFile("code.cpp");
        QFile inputXmlFile("inputXml.xml");

        QTextStream codeStream(&codeFile);
        QTextStream inputXmlStream(&inputXmlFile);

        if (!codeFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            qDebug() << "Can't create the file! Line: " << __LINE__;
            break;
        }
        else
        {
            codeFile.seek(0);
            codeStream << code;
            codeFile.close();
        }

        if (!inputXmlFile.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text))
        {
            qDebug() << "Can't create the file! Line: " << __LINE__;
            break;
        }

        QFile outputFileAux("output.txt");

        if (!outputFileAux.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            qDebug() << "Can't access the file! Line: " << __LINE__;
            continue;
        }
        outputFileAux.resize(0);
        outputFileAux.close();

        QFile inputFileAux("input.txt");

        if (!inputFileAux.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            qDebug() << "Can't access the file! Line: " << __LINE__;
            continue;
        }
        inputFileAux.resize(0);
        inputFileAux.close();

        inputXmlFile.seek(0);
        inputXmlStream << inputData;
        inputXmlFile.close();

        std::string filename = "inputXml.xml";
        pt::ptree inputTree;
        pt::ptree outputTree;

        pt::read_xml(filename, inputTree);

        QString errorMessage;

        int error = 0;

        QProcess process;
#ifdef __linux__
        process.start("g++ code.cpp -o code");
        process.waitForFinished(COMPILE_AND_RUN);

        errorMessage = process.readAllStandardError();
        qDebug() << "Error message: " << errorMessage;

        if (errorMessage.length() > 0)
        {
            error = 1;
        }
#elif _WIN32
        process.start("./mingw64/bin/g++ code.cpp -o code");
        process.waitForFinished(COMPILE_AND_RUN);

        errorMessage = process.readAllStandardError();
        qDebug() << "Error message: " << errorMessage;

        if (errorMessage.length() > 0)
        {
            error = 1;
        }
#endif

        BOOST_FOREACH(pt::ptree::value_type &v, inputTree.get_child("root"))
        {
            QFile inputFile("input.txt");
            QTextStream inputFileStream(&inputFile);

            if (!inputFile.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text))
            {
                qDebug() << "Can't access the file! Line: " << __LINE__;
                continue;
            }

            qDebug() << "Message: " + QString::fromStdString(v.second.data());
            inputFileStream << QString::fromStdString(v.second.data());
            inputFile.close();

            qDebug() << "Compiling and running...";
            QProcess process;

            if (!error)
            {
#ifdef __linux__
                process.start("./code");
                process.waitForFinished(COMPILE_AND_RUN);

                errorMessage = process.readAllStandardError();
                qDebug() << "Error message: " << errorMessage;

                if (errorMessage.length() > 0)
                {
                    break;
                }

#elif _WIN32
                process.start("code");
                process.waitForFinished(COMPILE_AND_RUN);

                errorMessage = process.readAllStandardError();
                qDebug() << "Error message: " << errorMessage;


                if (errorMessage.length() > 0)
                {
                    break;
                }
#else
                qDebug() << "What OS is this?";
#endif


                qDebug() << "After compile";

                QFile outputFile("output.txt");
                QTextStream outputDataStream(&outputFile);

                if (!outputFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    qDebug() << "Can't access the file! Line: " << __LINE__;
                    continue;
                }

                QString outputResult = outputDataStream.readAll();
                outputTree.add("root.output", outputResult.toStdString());

                outputFile.close();
                qDebug() << "Output: " + outputResult;
            }
        }

        if (errorMessage.length() > 0)
        {
            outputTree.add("root.output", "Error");
            outputTree.add("root.output", errorMessage.toStdString());
        }

        pt::write_xml("outputXml.xml", outputTree);

        QFile outputXmlFile("outputXml.xml");
        QTextStream outputXmlStream(&outputXmlFile);

        if (!outputXmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "Can't access the file! Line: " << __LINE__;
            continue;
        }
        outputData = outputXmlStream.readAll().toStdString().c_str();

        for (int i = 0; i < outputData.size(); i++)
        {
            if (outputData[i] == '\n')
            {
                outputData[i] = 15;
            }
        }
        outputXmlFile.close();

        qDebug() << "Output data: " << outputData;
    }
    while(false);
}

void LogicThread::sendData(QTcpSocket *socket, QByteArray data)
{
    socket->write(data);
    bool result = socket->waitForBytesWritten();
    msleep(100);

    if (result)
    {
        qDebug() << "Sent data.";
    }
    else
    {
        qDebug() << "Can't send data on the network! Line: " << __LINE__;
    }
}
