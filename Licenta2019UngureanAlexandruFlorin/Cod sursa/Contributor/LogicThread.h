#ifndef LOGICTHREAD_H
#define LOGICTHREAD_H

#include <QThread>
#include <QtNetwork/QTcpSocket>
#include <QFile>
#include <QProcess>
#include <QMessageBox>

#include "hardwareInfoThread.h"

#define COMPILE_AND_RUN 10000
#define READ_TIMEOUT 5000

class LogicThread : public QThread
{
public:
    static QString status;

    LogicThread();
    void run();
    void changeState();

private:
    void init();
    void loop();
    void getHardwareInfo();
    QByteArray intToByteArray(int input);
    void runCode();

    void sendData(QTcpSocket *socket, QByteArray data);

    QTcpSocket* contributorSocket;

    bool active;
    int ram;
    int cpu;

    HardwareInfoThread *info;

    QByteArray code;
    QByteArray inputData;
    QByteArray outputData;
};

#endif // LOGICTHREAD_H
