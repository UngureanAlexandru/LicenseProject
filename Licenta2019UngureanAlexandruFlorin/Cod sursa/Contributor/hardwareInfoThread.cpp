#include "hardwareInfoThread.h"

#include <QProcess>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include <QRegularExpression>

#define RAM_CPU_TIMEOUT 5000

HardwareInfoThread::HardwareInfoThread()
{
}

void HardwareInfoThread::run()
{
    while(true)
    {
        cpu = getCpuUsagePercentage();
        ram = getRamUsagePercentage();
        msleep(300);
    }
}

int HardwareInfoThread::getCPU()
{
    return cpu;
}

int HardwareInfoThread::getRAM()
{
    return ram;
}

int HardwareInfoThread::getCpuUsagePercentage()
{
    QProcess process;
#ifdef __linux__
    QFile getCPU("getCPU.sh");
    QTextStream ds(&getCPU);

    getCPU.open(QIODevice::WriteOnly);

    char* getCPUcommand = "top -b -n2 | grep \"Cpu(s)\" | awk '{print $2+$4}' | tail -n1";
    ds << getCPUcommand;
    getCPU.close();

    process.start("sh", QStringList() << "-c" << "./getCPU.sh");
    process.waitForFinished(RAM_CPU_TIMEOUT);
    QString result = process.readAllStandardOutput();
#elif _WIN32
    // windows code goes here
    QFile getCPU("getCPU.bat");
    QTextStream ds(&getCPU);

    getCPU.open(QIODevice::WriteOnly);

    char* getCPUcommand = "wmic cpu get loadpercentage";
    ds << getCPUcommand;
    getCPU.close();

    process.start("cmd.exe", QStringList() << "/C" << "getCPU.bat");
    process.waitForFinished(RAM_CPU_TIMEOUT);
    QString output = process.readAllStandardOutput();

    QRegularExpression regex("LoadPercentage\\s*(\\d*)\\s");
    regex.setPatternOptions(QRegularExpression::MultilineOption | QRegularExpression::DotMatchesEverythingOption);

    QString result = "0";

    QRegularExpressionMatch match = regex.match(output);
    if (match.hasMatch())
    {
       result = match.captured(1);
    }

    qDebug() << "CPU result: " << result;
#else
    qDebug() << "What OS is this?";
#endif
    return atoi(result.toStdString().c_str());
}

int HardwareInfoThread::getRamUsagePercentage()
{
    QProcess process;
#ifdef __linux__
    QFile getRAM("getRAM.sh");
    QTextStream ds(&getRAM);

    getRAM.open(QIODevice::WriteOnly);

    char* getRAMcommand = "free | grep Mem | awk '{print $3/$2 * 100.0}'";
    ds << getRAMcommand;
    getRAM.close();

    process.start("sh", QStringList() << "-c" << "./getRAM.sh");
    process.waitForFinished(RAM_CPU_TIMEOUT);
    QString result = process.readAllStandardOutput();
#elif _WIN32
    // windows code goes here
    QFile getRAM("getRAM.bat");
    QTextStream ds(&getRAM);

    getRAM.open(QIODevice::WriteOnly);

    char* getRAMcommand = "wmic OS get TotalVisibleMemorySize /Value && wmic OS get FreePhysicalMemory /Value";
    ds << getRAMcommand;
    getRAM.close();

    process.start("cmd.exe", QStringList() << "/C" << "getRAM.bat");
    process.waitForFinished(RAM_CPU_TIMEOUT);
    QString line = process.readAllStandardOutput();
    qDebug() << "Line: " << line;
    QRegularExpression regex(".*\\=(\\d*).*\\=(\\d*).*");
    regex.setPatternOptions(QRegularExpression::MultilineOption | QRegularExpression::DotMatchesEverythingOption);
    QString totalRam;
    QString availableRam;

    QRegularExpressionMatch match = regex.match(line);
    if (match.hasMatch())
    {
        totalRam = match.captured(1);
        availableRam = match.captured(2);
    }

//    qDebug() << "Total ram: " << atoi(totalRam.toStdString().c_str());
//    qDebug() << "Avalable ram: " << atoi(availableRam.toStdString().c_str());
//    qDebug() << "Used ram: " <<  atoi(totalRam.toStdString().c_str()) - atoi(availableRam.toStdString().c_str());

    double factor = atof(totalRam.toStdString().c_str()) / (atof(totalRam.toStdString().c_str()) - atof(availableRam.toStdString().c_str()));
    double number = 100 * 1 / factor;
    qDebug() << "Number: " << number;
    QString result = QString::number(static_cast<int>(number));
    qDebug() << "RAM result: " << result;
#else
    qDebug() << "What OS is this?";
#endif

    return atoi(result.toStdString().c_str());
}
