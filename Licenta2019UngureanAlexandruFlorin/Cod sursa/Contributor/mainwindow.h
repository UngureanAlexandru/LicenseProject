#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>

#include "LogicThread.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    static QString status;
    ~MainWindow();

private:
    QVBoxLayout *layout;
    QWidget *mainWidget;

    LogicThread *logicThread;
    QPushButton *onOffButton;
    QLabel *statusLabel;

    bool state;

private slots:
    void onOffFunction();
};

#endif // MAINWINDOW_H
