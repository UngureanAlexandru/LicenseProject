#ifndef HARDWAREINFOTHREAD_H
#define HARDWAREINFOTHREAD_H

#include <QThread>

class HardwareInfoThread : public QThread
{
public:
    HardwareInfoThread();

    int getCpuUsagePercentage();
    int getRamUsagePercentage();

    void run();
    int getCPU();
    int getRAM();

    int cpu;
    int ram;

};

#endif // HARDWAREINFOTHREAD_H
