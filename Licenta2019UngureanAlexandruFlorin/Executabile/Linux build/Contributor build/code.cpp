#include <fstream>
#include <iostream>

using namespace std;

int main()
{
	ifstream input;
	input.open("input.txt");

	ofstream output;
	output.open("output.txt");


        int nr;
        int prim = 1;

        input >> nr;

        for (int i = 3; i < nr; i++)
        {
            if (nr % i == 0)
            {
                prim = 0;
                break;
            }
        }

        output << prim;

	return 0;
}

